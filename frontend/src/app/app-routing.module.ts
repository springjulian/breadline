import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LandingComponent } from './landing/landing.component';
import { BlogComponent } from './blog/blog.component';
import { DonateComponent } from './donate/donate.component';
import { WhatWeDoComponent } from './what-we-do/what-we-do.component';

const routes: Routes = [
  { path: '', component: LandingComponent },
  { path: 'donate', component: DonateComponent },
  { path: 'what-we-do', component: WhatWeDoComponent },

  { path: 'blog/:slug', component: BlogComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
