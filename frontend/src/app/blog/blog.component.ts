import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { StrapiService, Page } from '../services/strapi.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {

  blog: Page;

  constructor(
    private strapiService: StrapiService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {

    this.activatedRoute.paramMap.subscribe((param) => {
      console.log(param.get('slug'));
      this.strapiService.getPage(param.get('slug')).subscribe((page: Page) => {
        this.blog = page;
      });
    });
  }

}
