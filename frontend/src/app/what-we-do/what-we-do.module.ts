import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WhatWeDoComponent } from './what-we-do.component';



@NgModule({
  declarations: [WhatWeDoComponent],
  imports: [
    CommonModule
  ]
})
export class WhatWeDoModule { }
