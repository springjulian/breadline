import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

export interface Image {
  name: string;
  width: number;
  height: number;
  url: string;
}

export interface Page {
  Headline: string;
  body: string;
  headlinePicture: Image;
  slug: string;
}

@Injectable({
  providedIn: 'root'
})
export class StrapiService {

  private strapiUrl: string = 'http://localhost:1337'

  constructor(private http: HttpClient) { }

  getPages(): Observable<Page[]> {
      return this.http.get<Page[]>(`${this.strapiUrl}/pages`);
  }

  getStrapiUrl(): string {
    return this.strapiUrl;
  }

  getPage(slug: string): Observable<Page> {
    return this.http.get<Page>(`${this.strapiUrl}/pages/${slug}`);
  }
}
