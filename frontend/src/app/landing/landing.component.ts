import { Component, OnInit } from '@angular/core';
import { StrapiService, Page } from '../services/strapi.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  landingPages: Page[];
  stripiUrl: string;

  constructor(private strapiService: StrapiService) { }

  ngOnInit(): void {
    this.getPages();
    this.getStrapiUrl();
  }

  getPages(): void {
    this.strapiService.getPages().subscribe((pages: Page[]) => {
      this.landingPages = pages;
    })
  }

  getStrapiUrl(): void {
    this.stripiUrl = this.strapiService.getStrapiUrl()
  }


}
