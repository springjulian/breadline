import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingModule } from './landing/landing.module';
import { BlogModule } from './blog/blog.module';
import { DonateModule } from './donate/donate.module';
import { WhatWeDoModule } from './what-we-do/what-we-do.module';

import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    LandingModule,
    BlogModule,
    MatMenuModule,
    MatButtonModule,
    DonateModule,
    WhatWeDoModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
